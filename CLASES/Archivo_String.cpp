#include <stdlib.h>
#include <stdio.h>
//#include <conio>
#include<cstdio>
#include <iostream>
#include <string.h>

using namespace std;
/*
    FunciOn     Significado
   +----------------------------------------------------------------------------+
    fopen()     Abre archivo una ristra
    fclose()    Cierra una ristra
    putc()      Escribe un caracter en una ristra
    getc()      Lee un caracter de una ristra
    fseek()     Busca un byte especIfico en una ristra
    fprintf()   Es a una ristra lo que printf() es a la consola
    fscanf()    Es a una ristra lo que scanf() es a la consola
    feof()      Devuelve verdadero si se alcanzO el fin de archivo
    ferrorf     Devuelve verdadero si ha ocurrido un error
    rewind()    Restablece la pocisiOn del apuntador de archivo al inicio del mismo
    remove()    Borra un archivo
    fgets()

   +----------------------------------------------------------------------------+
    Modo        Significado
   +----------------------------------------------------------------------------+
    "r"         Abre un archivo de texto para lectura
    "w"         Abre un archivo de texto para escritura
    "a"         Agrega (al final) a un archivo de texto
    "rb"        Abre un archivo binario para lectura
    "wb"        Crea un archivo binario para escritura
    "ab"        Agrega (al final) a un archivo binario
    "r+"        Abre un archivo de texto para escritura/lectura
    "w+"        Crea un archivo de texto para escritura/lectura
    "a+"        Abre o crea un archivo de texto para escritura/lectura
    "r+b"       Abre un archivo binario para escritura/lectura
    "w+b"       Crea un archivo binario para escritura/lectura
    "a+b"       Abre o crea un archivo binario escritura/lectura
    "rt"        Abre un archivo de texto para lectura
    "wt"        Crea un archivo de texto para escritura
    "at"        Agrega (al final) a un archivo de texto
    "r+t"       Abre un archivo de texto para escritura/lectura
    "w+t"       Crea un archivo de texto para escritura/lectura
    "a+t"       Abre o crea un archivo de texto para escritura(al final)/lectura
   +----------------------------------------------------------------------------+
*/
using namespace std;
//
class CsCadenas {
public:
    CsCadenas( char [ ], char [ ] );
    void AbreAvoFa( void );
    void EscribeAvoDiscoFa( void );
    void LeeAvoDiscoFnApCd( void );
    void RebobinaAvoFa( void ) { rewind( pApFILE ); }
    void CierraAvoFa( void ) { fclose( pApFILE ); }
    char **SeparaPalabrasFaApApCd(char *);
    void ExhibeFn(void);
private:
    FILE *pApFILE;
    char NombAvoCd[ 12 ]; 
    char ModoAvoCd[ 6 ];
    char **pApApCd;
    int jEn;
};
/*
    El constructor inicia las dos variables asociadas: la variable NombAvoCd con
    el nombre del archivo en disco y, la variable ModoAvoCd con el modo de 
    apertura del mismo archivo.
*/
CsCadenas::CsCadenas( char nombAvoCd[ ], char modoAvoCd[ ] ) {
    strcpy(NombAvoCd, nombAvoCd);
    strcpy(ModoAvoCd, modoAvoCd);
    pApApCd = new char *[32];
}
/*
    La funciOn asociada AbreAvoFa, abre un archivo mediante la funciOn fopen, la
    cual asocia el nombre del archivo, en la variable NombAvoCd, y su modo, en 
    la variable ModoAvoCd, con el apuntador de archivo pApFILE. El archivo en 
    disco se usa a traves de este apuntador.
*/
void CsCadenas::AbreAvoFa( void ) {
    if( ( pApFILE = fopen( NombAvoCd, ModoAvoCd ) ) == NULL ) {
        printf( "Archivo no abre" );
        exit( 1 );
    }
}
/*
    La funciOn asociada EscribeAvoDiscoFa, lee una cadena de hasta 127 caracteres
    de teclado con un <-' al final de la misma. Luego se escribe a disco la cadena,
    enseguida se digita $<-' para indicar fin de cadena. A la cadena, al final, 
    se le inserta un cambio de lInea y un fin de cadena antes de escribirse a disco.
    La funciOn puede leer tantas cadenas como sea necesario.
*/
void CsCadenas::EscribeAvoDiscoFa( void ) {
    char TextoCd[ 128 ] = "";
    int LenEn;


      fgets(TextoCd,128,stdin);//Lectura de teclado
 //cout<<TextoCd<<endl;
 fputs( TextoCd, pApFILE );            // Escritura a disco

//REVISAR Y CORREGIR
/*      while( strcmp( TextoCd, "$" ) != 0 ) {
        LenEn = strlen(TextoCd);
        TextoCd[ LenEn ] = char( 10 );        //Cambio de lInea en codigo ASCII
        TextoCd[ LenEn + 1 ] = '\0';          //Fin de cadena
        fputs( TextoCd, pApFILE );            // Escritura a disco
       fgets(TextoCd,24,stdin);
    }
*/

}

/*
    La funciOn asociada LeeAvoDiscoFa lee del archivo en disco, un texto de hasta
    128 caracteres en la variable TextoApCd y la exhibe en el monitor, luego lee
    el siguiente texto y asI sucesivamente hasta el Ultimo en el archivo.
*/

void CsCadenas::LeeAvoDiscoFnApCd( void ) {
    char *TextoApCd = new char [ 128 ];

    fgets(TextoApCd,128,pApFILE);            // Lectura de disco
    while( !feof( pApFILE ) ) {                 // !feof( pApFILE ) es igual a CaraCa != EOF;    donde EOF = -1
        /*pApApCd = SeparaPalabrasFaApApCd( TextoApCd );
        for(int iEn = 0; iEn <= jEn; iEn++) 
             cout << pApApCd[iEn] << endl;
        jEn = 0; */  //cout << TextoApCd;
        cout << TextoApCd << endl;
        fgets( TextoApCd, 128, pApFILE );       // Lectura de disco
    }
}
//
char **CsCadenas::SeparaPalabrasFaApApCd(char *TextoApCd) {
    char CaraCa, PalabraCd[64];   //Antes el arreglo era de 16 caracteres
    int iEn = 0, kEn = 0, LonEn;
    jEn = 0;
    while((CaraCa = TextoApCd[iEn++]) != '\0'){
        if(CaraCa != ' '){
            PalabraCd[kEn++] = CaraCa;
            //cout << CaraCa;
        }else{
            PalabraCd[kEn] = '\0';
            LonEn = strlen(PalabraCd) + 1;
            pApApCd[jEn++] = new char [LonEn];
            strcpy(pApApCd[jEn - 1], PalabraCd);
            kEn = 0;
            //cout << PalabraCd << endl;
            puts("");
        }
    }
    PalabraCd[kEn] = '\0';
    LonEn = strlen(PalabraCd) + 1;
    pApApCd[jEn] = new char [LonEn];
    strcpy(pApApCd[jEn], PalabraCd);
    //cout << PalabraCd << endl;
    
    return pApApCd;
}
//
void CsCadenas::ExhibeFn(void) {
    for(int iEn = 0; iEn < jEn; iEn++)
        cout << pApApCd[iEn] << endl;
}
//
main( void ) {
    char NombAvoCd[ ] ="Texto.txt";
    char ModoAvoCd[ ] = "w+";                      // Crea un archivo para lectura/escritura
    CsCadenas AvoCsCadenas( NombAvoCd, ModoAvoCd );
    
    AvoCsCadenas.AbreAvoFa( );
    
    puts("+------------------------------------+");
    cout << "| COMO INGRESAR UN TEXTO             |\n";
    puts("+------------------------------------+");
    cout << "| 1.-  Ingrese un texto y pulse <-'  |\n";
//  cout << "| 2.-  luego ingrese $ y pulse  <-'  |\n";
    puts("+------------------------------------+");
    puts("\n");

    AvoCsCadenas.EscribeAvoDiscoFa( );
    AvoCsCadenas.RebobinaAvoFa( );
                                                
    char intermedio[256] = AvoCsCadenas.LeeAvoDiscoFnApCd( );
   // AvoCsCadenas.CierraAvoFa( );

 AvoCsCadenas.SeparaPalabrasFaApApCd(intermedio);
   //AvoCsCadenas.SeparaPalabrasFaApApCd(AvoCsCadenas.LeeAvoDiscoFnApCd( ));
   //Devuelve una cadena en la que se concatena lInea por lInea leida de disco
   AvoCsCadenas.ExhibeFn();
   AvoCsCadenas.CierraAvoFa();
    


//    system( "pause" );
}

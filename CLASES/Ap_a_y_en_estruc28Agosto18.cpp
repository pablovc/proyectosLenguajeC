//Apuntadores a y en Estructuras

#include<stdio.h>
#include<stdlib.h>
#include<iostream>

using namespace std;


/*
Una estructura (struct) es una estructura de datos heterogenea, es decir, cada entrada puede ser de tipo deiferente 
//C  $ struct EsDatosLosEsDatos={'A',100};
//C++$ EsDatos Los EsDatos={'A',100};
//Es importante notar que al escribir estructuras es importante finalizarlas con ';'
*/


// *CreaFnApAr(EsDatos(void))[2];/

struct EsDatos{
   char AlfaCa;
   int ValorEn;

};

//####DECLARACIONES####

//void ExhibeFn(EsDatos (*pApArEsDatos)[2]);
//EsDatos(*CreaFnApArEsDatos(void))[2];



EsDatos(*CreaFnApArEsDatos(void))[2]{ 
  //####TERCERA PARTE####
  EsDatos(*pApArEsDatos)[2]=(EsDatos(*)[2])new EsDatos[2];

/*
              +-----+
pApArEsDatos->| ... |
              +-----+
              | ... |
              +-----+
*/

    (*pApArEsDatos)[0].AlfaCa='A';
    (*pApArEsDatos)[0].ValorEn=10;  
    (*pApArEsDatos)[1].AlfaCa='B';
    (*pApArEsDatos)[1].ValorEn=100;
   
     return pApArEsDatos;
}

//SI FUNCIONO EL PROGRAMA
void ExhibeFn(EsDatos (*pApArEsDatos)[2]){

    for(int iEn=0;iEn<2;iEn++){
              cout<<(*pApArEsDatos)[iEn].AlfaCa<<" "<<(*pApArEsDatos)[iEn].ValorEn<<endl;
    }

}

int main(void){

//####PARTE CERO####
//   EsDatos LosEsDatos={'A',100};
//    cout<<LosEsDatos.AlfaCa<<endl;
//    cout<<LosEsDatos.ValorEn<<endl;
  

//####PRIMERA PARTE#####

/*
//notaciones equivalentes
pApEsDatos -> AlfaCa = 'B';
(*pApEsDatos).AlfaCa = 'B';
*/

//EsDatos*pApEsDatos = &LosEsDatos;
//SE COMPRUEBA QUE AMBAS NOTACIONES SON EQUIVALENTES
//pApEsDatos->AlfaCa='B';
//pApEsDatos->ValorEn=50;
//(*pApEsDatos).AlfaCa='B';
//(*pApEsDatos).ValorEn=50;
//cout<<pApEsDatos->AlfaCa<<" "<<pApEsDatos->ValorEn<<endl;

//####SEGUNDA PARTE####

//EsDatos *qApEsDatos = new EsDatos;
  // qApEsDatos->AlfaCa = 'X';
  // qApEsDatos->ValorEn = 30;
//Se hace notar que ambas notaciones son compatibles entre sI  
//cout<<(*qApEsDatos).AlfaCa<<" "<<qApEsDatos->ValorEn<<endl;

//####CUARTA PARTE####

ExhibeFn(CreaFnApArEsDatos());

return 0;
}
	

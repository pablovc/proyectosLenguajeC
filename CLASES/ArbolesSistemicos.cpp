//#### PREAMBULO ####
#include<stdio.h>
#include<iostream>
#include<stdlib.h>

using namespace std;

/*
Un arbol sistemico tiene nodos con  <,=,> y hojas con acciones

Tenemos 3 variables enteras: XEn, YEn, y ZEn. Se exhibe la menor o, dos de ellas si son iguales y menor a ala tercero, se exhiben las tres si son iguales

*/

/*
NIVELES DE LA FUNCION Compara3ValoresFn

                     if                              else if                              else
                 (XEn<YEn)                           (XEn=YEn)                          (XEn>YEn)
            /        |        \                 /        |         \               /        |           \
           /         |         \               /         |          \             /         |            \
          if      else if     else            if      else if      else          if      else if         if
       (XEn<ZEn) (XEn=ZEn)   (XEn>ZEn)    (XEn=ZEn)  (XEn=ZEn)   (XEn>ZEn)    (YEn<ZEn)  (YEn=ZEn)    (YEn>ZEn)
*/

//#### COMIENZA EL CODIGO ####

void Compara3ValoresFn(int XEn,int YEn, int ZEn){
   //Seccion 1 de 3
   if(XEn<YEn){
           if(XEn<ZEn){
                  cout<<"XEn:"<<XEn<<endl;
           }
           else if(XEn==ZEn){
                  cout<<"XEn:"<<XEn<<"ZEn:"<<ZEn<<endl;
           }
           //(XEn>>ZEn)
           else{
                  cout<<"ZEn:"<<ZEn<<endl;
           }
    }
    //Seccion 2 de 3
   else if(XEn=YEn){
           if(XEn<ZEn){
                  cout<<"XEn:"<<"YEn:"<<YEn<<endl;
           }
           else if(XEn==ZEn){
                   cout<<"XEn:"<<XEn<<"YEn:"<<YEn<<"ZEn:"<<ZEn<<endl;     
           }
           //(XEn>ZEn)
           else{
           
                  cout<<"ZEn:"<<ZEn<<endl;
           }
   }
    //Seccion 3 de 3
   // else
    // if(XEn>YEn)
    else{
    
           if(YEn<ZEn){
                  cout<<"YEn:"<<YEn<<endl;
           }
           else if(YEn==ZEn){
                   cout<<"YEn:"<<YEn<<"ZEn:"<<ZEn<<endl;     
           }
           //(YEn>ZEn)
           else{
                  cout<<"ZEn:"<<ZEn<<endl;
           }
   }
    
//Fin de las secciones

}
//Fin de la funcion Compara3ValoresFn


int main (void){
int aEn, bEn, cEn;
char opcionCa;
do{
   puts("Ingresa 3 valores enteros");
    cin>>aEn>>bEn>>cEn;
      Compara3ValoresFn(aEn,bEn,cEn);
     cout<<"otroEjemplo:";
      cin>>opcionCa;
}
while(opcionCa=='s');


return 0;
}

//#### PREAMBULO ####
#include<stdio.h>
#include<iostream>
#include<stdlib.h>
//Caracteres y cadenas
#include<string.h>

using namespace std;

//#### DOCUMENTACION ####

/*
1.- Una clase representa la abstraccion de una categoria de objetos

Ejemplo de una categorIa de objetos:Sillas,Autos,Arboles,Plumas,Celulares,Libretas

LOS OBJETOS SON SUSTANTIVOS
Autos:
Atributos ejemplo:
Ruedas{4,3}
Motor{Electrico,Mecanico,Hibrido}
Puertas{2,4}
Volante{Izquierda,Derecha}
Asientos{4,2}
Fuente{Gasolina, Diesel,Celdas,Gas}

Comportamiento Auto:
*Arranca
*Reversa
*Acelera
*Frena
...

Ventanas:
Atributos:
*Simple
*Doble
*Doble Horizontal
*Doble Vertical

       x_o
      |
 y_o__|_____
      |     |
      |     |
      |_____|__y_f
            |
            |
           x_f

Posiciones iniciales y finales de las ventanas -> x_o,y_o,x_f,y_f

Comprtamiento:
*Ocultar
*Exhibir
*Trasladar
*Escalar

*/



class CsLibros{

public:

//El constructor ni el destructor tienen tipo de DATO
 //void IniciaFa(char[16],char[16]);
// CsLibros(char[16],char[16],int[10]);
CsLibros(char*,char*,int);
  void ExhibeFa(void);

//DESTRUCTOR (no tiene argumentos)
~CsLibros(void);
private:
// CsLibros(int);
 // char AutorCd[16];
 // char TituloCd[16];
  char *AutorApCd;
  char *TituloApCd;
   int NoEjemplaresEn;

};
//Funciones

//void CsLibros::IniciaFa(char autorCd[16],char tituloCd[16]){

/*
Constructor-> crea el objeto 
  Funciones:
 1.- Inicia las variables asociadas a la clase con valores
 2.- Recupera memoria para variables diamicas
 Caracteristicas
 1.-No tiene tipo
 2.-Puede tener o no objetos
 3.-Puede tener homonimos
*/

CsLibros::CsLibros(char *autorApCd, char *tituloApCd,int NoEjemplaresEn){

  AutorApCd=autorApCd;
 TituloApCd=tituloApCd;
 this->NoEjemplaresEn=NoEjemplaresEn;

}

void CsLibros::ExhibeFa(void){
   cout<<AutorApCd<<endl;
   cout<<TituloApCd<<endl;
   cout<<NoEjemplaresEn<<endl;

}


/*
Destructor
 1.- Recupera memoria cedida a variables dinamicas
 2.- Destruye el objeto
 Caracteristicas
 1.-No tiene tipo
 2.-No tiene argumentos
*/

CsLibros::~CsLibros(void){
  ;
}


//FunciOn principal
int main (void){

//CsLibros ElCsLibros;

CsLibros ElCsLibros("Pepe","fisica",10);
ElCsLibros.ExhibeFa();

CsLibros El2CsLibros("Jose","quimica",20);
El2CsLibros.ExhibeFa();

return 0;
}


#include<iostream>
#include<stdlib.h>

using namespace std;

//###FUNCIONES###

void ExhibeFn(int *(*(*pApArFnApEn)[4])(int,int));
int*(*(*CreaFnApArApFnApEn(void))[4])(int,int);
int*SumaFnApEn(int,int);
int*RestaFnApEn(int,int);
int*ProductoFnApEn(int,int);
int*DivisionFnApEn(int,int);


/*
Especificaciones

CreaFnApArApFnApEn

Fn=SumaFnApEn;
   RestaFnApEn;
   ProductoFnApEn;
   DivisionFnApEn;
*/

int*(*(*CreaFnApArApFnApEn(void))[4])(int,int){

  //Se hace un apuntador a un arreglo de apuntadores que se dirigen a las funciones "suma, resta,etc,..."
  int*(*(*pApArApFnApEn)[4])(int,int)=(int*(*(*)[4])(int,int))new(int*(*[4])(int,int));

  //se asigna una funcion a  cada elemento del arreglo de apuntadores a las"operaciones"
  //se llamaran operaciones a: suma resta, etc, ..
  
  (*pApArApFnApEn)[0]=SumaFnApEn;
  (*pApArApFnApEn)[1]=RestaFnApEn;
  (*pApArApFnApEn)[2]=ProductoFnApEn;
  (*pApArApFnApEn)[3]=DivisionFnApEn;

/*

//En arreglo es dinamico
    +---+
p-> | * |->SumaFnApEn
    +---+
    | * |->RestaFnApEn
    +---+
    | * |->ProductoFnApEn
    +---+
    | * |->DivisionFnApEn
    +---+
*/

}


//####OPERACIONES#####

int *RestaFnApEn(int AEn,int BEn){
   int *pApEn=new int;
   *pApEn=AEn-BEn;
   return pApEn;
}

int *ProductoFnApEn(int AEn, int BEn){
    int *pApEn=new int;
    *pApEn=AEn*BEn;
    return pApEn;
}

int *DivisionFnApEn(int AEn, int BEn){
    int *pApEn=new int;
    *pApEn=AEn/BEn;
    return pApEn;
}


int *SumaFnApEn(int AEn,int BEn){
  int *pApEn=new int;
  *pApEn=AEn+BEn;
   return pApEn;
}

void ExhibeFn(int *(*(*pApArFnApEn)[4])(int,int)){
    for(int iEn=0;iEn<4;iEn++)
    cout<<*(*(*pApArFnApEn)[iEn])(1,2)<<endl;
}


main (void){

    ExhibeFn(CreaFnApArApFnApEn());

}


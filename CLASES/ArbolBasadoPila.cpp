//#### PREAMBULO ####
#include<stdio.h>
#include<iostream>
#include<stdlib.h>

using namespace std;

/*
Pilas
Una pila es una estructura de datos lineal con dos operaciones por un mismo extremo: agregar y remover

PilaArEn

int PilaArEn[6];

    +---+
n-1 |   | <-TopEn=n-1
    +---+
    |...|
    +---+
    |101|<-TopeEn
    +---+
    |...|
    +---+
    |101|<-TopeEn=-1
    +---+
 
*/

//Variables globales
/*
int TopeEn=-1;
int PilaArEn[12];
int NoMaxItemsEn=12;
*/

struct EsNodo{
    char CaraCa;
    EsNodo *LigaIzqApEsNodo;
    EsNodo *LigaDerApEsNodo;
}

class CsArboles{

public:
    void AgregaFaFn(int);
    int RemueveEnFaFn(void);
   void ExhibePilaFnFa(void);
    CsArboles(void);
private: 
   int TopeEn;
   int PilaArEn[12];
   int NoMaxItemsEn;
//FUNCIONES DE EXAMEN
 //int *CreaFnApEn(void);
  //bool EsVaciaFaBo(void);
  //bool EsLlenaBo(void);
};
//Fin de la clase

//Constructor agrega valores
CsArboles::CsArboles(void){
   TopeEn=-1;
   NoMaxItemsEn=12;
}

//int *CreaFnApEn(void);


//EXAMEN PARCIAL MARTES
//REALIZAR EL TOPE ENTERO CON APUNTADORES
/*

        +---+
TopeEn->| * |->1  CraFnApEn(void){Ingresar entero}
        +---+
        | * |->7
        +---+
        |...|
        +---+
*/


//


EsNodo  CsArboles::*AgregaFnApEsNodo(EsNodo *ProApEsNodo){
	EsNodo *tApEsNodo=ProApEsNodo;
	EsNodo *pApEsNodo= new EsNodo;

	puts("\nIngresa un caracter:");
	cin>> pApEsNodo->CaraCa;

//Lista circular

while(tApEsNodo->SigApEsNodo!=ProApEsNodo){
	tApEsNodo=tApEsNodo->SigApEsNodo;
	}
	tApEsNodo->SigApEsNodo=pApEsNodo;
	pApEsNodo->SigApEsNodo=ProApEsNodo;	
	return ProApEsNodo;



}

//Funcion Asociada de agregar elementos
void CsArboles::AgregaFaFn(int AlfaEn){
  if(TopeEn!=NoMaxItemsEn-1){
    PilaArEn[++TopeEn]=AlfaEn;
    }
   else{
        cout<<"Pila desbordada"<<endl;
        exit(1);
   }

}

//Funcion Asociada de destruccion de elementos
int CsArboles::RemueveEnFaFn(void){
    //if(EsVaciaFnBo());
    if(TopeEn!=-1){
      return PilaArEn[TopeEn--];
    }
    else{
      cout<<endl;
      cout<<"Pila vacia"<<endl;
      exit(1);
    }
} 

//Funcion Asociada a la creacion del arbol
void CsArboles::generaArbolFnApEsNodo(char *ExpAriApCd){
   int iEn;
   while((CaraCa=ExpAriApCd[iEn++])!='\0'){
      if(EsOpdoFnBo(CaraCa)==true){
        pApEsNodo=new EsNodo;
        pApEsNodo->CaraCa=CaraCa;
        pApEsNodo->LigaIzqApEsNodo=pApEsNodo->LigaDerApEsnodo=NULL;
        AgregaFaFn(pApEsNodo);
     }
     else if(EsOpdorFnBo(CaraCa)==true){
       LigaDerApEsNodo=RemueveFnApEsNodo();
       LigaIzqApEsNodo=RemueveFnApEsNodo();
       pApEsNodo->LigaIzqApEsNodo=LigaIzqApEsNodo;
       pApEsNodo->LigaDerApEsNodo=LigaDerApEsNodo;
       AgregaFaFn(pApEsNodo);
     }
   }
  
}
  

void CsArboles::ExhibePilaFnFa(void){
    //se copia valor de TopeEn original para no moverlo
     int Tope0En=TopeEn;
     cout<<"+---+"<<endl;
     cout<<"| "<<PilaArEn[Tope0En--]<<" |"<<"<-TopeEn"<<endl;
     cout<<"+---+"<<endl;
     for(int iEn=Tope0En;iEn>-1;iEn--){
       cout<<"| "<<PilaArEn[Tope0En--]<<" |"<<endl;
       cout<<"+---+"<<endl; 
     }
}


int main (void){

//Creacion de objeto
CsArboles ElArbol;


//CASO PILA Ejemplo
 int NumeroElementos;
 int RemueveElementos; 
char opcionCa;

//CASO PILA VACIA
//int NumeroElementos=4;
//int RemueveElementos=5;


do{

//Borra pantalla
system("clear");

cout<<"Numero de Elementos:";
cin>>NumeroElementos;
cout<<endl;

cout<<"Elementos a remover:";
cin>>RemueveElementos;
cout<<endl;

//Borra pantalla
system("clear");

//Se agregan elementos
for(int iEn=0;iEn<NumeroElementos;iEn++)
   LaPilaEn.AgregaFaFn(iEn);


//Se exhibe la pila con todos los elementos
LaPilaEn.ExhibePilaFnFa();

//salto de linea para visibilidad
cout<<endl;

//Se borran elementos
for(int iEn=0;iEn<RemueveElementos;iEn++)
//LaPilaEn.RemueveEnFaFn();
 cout<<LaPilaEn.RemueveEnFaFn()<<"";


//salto de linea para visibilidad
cout<<endl;

//Se exhibe la pila con elementos elmiminados
LaPilaEn.ExhibePilaFnFa();

//Aqui esta la impresion provisional de la pila removida
  // cout<<LaPilaEn.RemueveEnFaFn()<<"";

//Se borra pantalla
//system("clear");
cout<<"otro ejemplo:";
cin>>opcionCa;
}while(opcionCa=='s');



//salto de linea para visibilidad
cout<<endl;
return 0;
}



//######A PARTIR DE AQUI EMPIEZA ARBOL ORIGINAL DE CLASE######

//#include<iostream>
//#include<stdio.h>
//#include"pila.cpp"
//#include<stdlib.h>

//using namespace  std;

/*
struct EsNodo{
    char CaraCa;
    EsNodo *LigaIzqApEsNodo;
    EsNodo *LigaDerApEsNodo;
}
*/

/*
generaArbolFnApEsNodo(char *ExpAriApCd){
   int iEn;
   while((CaraCa=ExpAriApCd[iEn++])!='\0'){
      if(EsOpdoFnBo(CaraCa)==true){
        pApEsNodo=new EsNodo;
        pApEsNodo->CaraCa=CaraCa;
        pApEsNodo->LigaIzqApEsNodo=pApEsNodo->LigaDerApEsnodo=NULL;
        AgregaFn(pApEsNodo);
     }
     else if(EsOpdorFnBo(CaraCa)==true){
       LigaDerApEsNodo=RemueveFnApEsNodo();
       LigaIzqApEsNodo=RemueveFnApEsNodo();
       pApEsNodo->LigaIzqApEsNodo=LigaIzqApEsNodo;
       pApEsNodo->LigaDerApEsNodo=LigaDerApEsNodo;
       AgregaFn(pApEsNodo);
     }
   }
  
}
*/


//####PROGRAMA LISTA CIRCULAR####

/*

#include<iostream>
#include<stdio.h>

using namespace std;

struct EsNodo{
	//CaraCa es el campo del nodo selnialado por qApEsNodpo
	char CaraCa;
	EsNodo *SigApEsNodo;
};


EsNodo *IniciaFnApEsNodo(void){
	EsNodo *ProApEsNodo = new EsNodo;

	puts ("\nIngresa un caracter:");
	cin>>ProApEsNodo->CaraCa;
//Lista enlazada
//	ProApEsNodo->SigApEsNodo=NULL;
//Lista circular
	ProApEsNodo->SigApEsNodo=ProApEsNodo;

	return ProApEsNodo;
}


EsNodo *AgregaFnApEsNodo(EsNodo *ProApEsNodo){
	EsNodo *tApEsNodo=ProApEsNodo;
	EsNodo *pApEsNodo= new EsNodo;

	puts("\nIngresa un caracter:");
	cin>> pApEsNodo->CaraCa;

//Lista circular

while(tApEsNodo->SigApEsNodo!=ProApEsNodo){
	tApEsNodo=tApEsNodo->SigApEsNodo;
	}
	tApEsNodo->SigApEsNodo=pApEsNodo;
	pApEsNodo->SigApEsNodo=ProApEsNodo;	
	return ProApEsNodo;



}



EsNodo *AgregaInicioFnApEsNodo(EsNodo *ProApEsNodo){

       EsNodo *oApEsNodo=ProApEsNodo;
	EsNodo *rApEsNodo=new EsNodo;
	EsNodo *sApEsNodo=ProApEsNodo;
	
	puts("Ingresa un caracter: ");
	cin>>rApEsNodo->CaraCa;
	while(sApEsNodo->SigApEsNodo!=ProApEsNodo){
		sApEsNodo=sApEsNodo->SigApEsNodo;
	}
	rApEsNodo->SigApEsNodo=oApEsNodo;
	ProApEsNodo=rApEsNodo;
	sApEsNodo->SigApEsNodo=ProApEsNodo;
	return ProApEsNodo;

}



void ExhibeFn(EsNodo *ProApEsNodo){

//lista circular
EsNodo *tApEsNodo = ProApEsNodo;
	if(tApEsNodo->SigApEsNodo==ProApEsNodo)
		cout<<tApEsNodo->CaraCa;
	else
		do{
			cout<<tApEsNodo->CaraCa;
			tApEsNodo=tApEsNodo->SigApEsNodo;
		}
		while(tApEsNodo!=ProApEsNodo);

}


EsNodo *SuprimeNodoInicialFnApEsNodo(EsNodo *ProApEsNodo){

//lista circular
EsNodo *pApEsNodo=ProApEsNodo;
	EsNodo *sApEsNodo=ProApEsNodo;
	while(sApEsNodo->SigApEsNodo!=ProApEsNodo){
		sApEsNodo=sApEsNodo->SigApEsNodo;
	}
	ProApEsNodo=ProApEsNodo->SigApEsNodo;
	sApEsNodo->SigApEsNodo=ProApEsNodo;
	pApEsNodo->SigApEsNodo=NULL;
	delete pApEsNodo;
	return ProApEsNodo;

}


EsNodo *SuprimeNodoFinalFnApEsNodo(EsNodo *ProApEsNodo){

//lista circular
EsNodo *pApEsNodo=ProApEsNodo;
	EsNodo *rApEsNodo=ProApEsNodo;
	while(pApEsNodo->SigApEsNodo!=ProApEsNodo){
		pApEsNodo=pApEsNodo->SigApEsNodo;	
	}
	while(rApEsNodo->SigApEsNodo!=pApEsNodo){
		rApEsNodo=rApEsNodo->SigApEsNodo;
	}
	rApEsNodo->SigApEsNodo=ProApEsNodo;
	pApEsNodo->SigApEsNodo=NULL;
	delete pApEsNodo;
	return ProApEsNodo;

}


EsNodo *SuprimeNodoMedioFnApEsNodo(EsNodo *ProApEsNodo, int pos){

	EsNodo *pApEsNodo=ProApEsNodo;
	EsNodo *qApEsNodo= new EsNodo;
	int ContaNodos=0;

	if(pos==0){
		return SuprimeNodoInicialFnApEsNodo(pApEsNodo);
	}

	if(pos<0){
		cout<<"Posicion no vAlida"<<endl;
		return pApEsNodo;
	}

	while(pApEsNodo->SigApEsNodo!=ProApEsNodo&&ContaNodos!=pos){
		qApEsNodo=pApEsNodo;
		pApEsNodo=pApEsNodo->SigApEsNodo;
		ContaNodos++;
	}

	qApEsNodo->SigApEsNodo=pApEsNodo->SigApEsNodo;
	delete pApEsNodo;

	return ProApEsNodo; 
}


EsNodo *AgregaNodoMedioFnApEsNodo(EsNodo *ProApEsNodo, int pos){

	EsNodo *pApEsNodo=ProApEsNodo;
	EsNodo *qApEsNodo= new EsNodo;

	int ContaNodos=0;

	puts("\nIngresa un caracter:");
	cin>> qApEsNodo->CaraCa;

	while(pApEsNodo->SigApEsNodo!=ProApEsNodo&&ContaNodos!=pos){
		pApEsNodo=pApEsNodo->SigApEsNodo;
		ContaNodos++;
	}

	qApEsNodo->SigApEsNodo=pApEsNodo->SigApEsNodo;
	pApEsNodo->SigApEsNodo=qApEsNodo;

	return ProApEsNodo; 
}




main(void){
	int tamEn;	// Referencia temporal para crear la lista

       cout<<"Programa Listas"<<endl;
	
	cout<<"Ingresa tamanio de lista"<<endl;
	cin>>tamEn;

        cout<<"Ingresa caracter por caracter los elementos de la lista"<<endl;

	EsNodo *ProApEsNodo=IniciaFnApEsNodo();

	for(int iEn=0; iEn<tamEn; iEn++){
		AgregaFnApEsNodo(ProApEsNodo);
	}

        cout<<"La nueva lista es:"<<endl;
	ExhibeFn(ProApEsNodo);
	cout<<endl;
       
          
        
	char opcionCa;
        int menuOpcion;
        int posicion;

	do{
        //borrar pantalla
        cout << "\033[2J\033[1;1H";
        ExhibeFn(ProApEsNodo);
	cout<<endl;
	cout<<"Menu de opciones"<<endl;
	cout<<"1 -> suprimir nodo al inicio"<<endl;
	cout<<"2 -> suprimir nodo al final"<<endl;
	cout<<"3 -> agregar nodo al inicio"<<endl;
	cout<<"4 -> agregar nodo al final"<<endl;
	cout<<"5 -> suprimir nodo en posicion N"<<endl;
	cout<<"6 -> agregar nodo en posicion N"<<endl;
	cout<<endl; 
        cout<<"Ingresa entero referido al menu de opciones"<<endl;
	cin>>menuOpcion;
       //system(clear);
	    switch(menuOpcion){
                case 1:
                        ProApEsNodo=SuprimeNodoInicialFnApEsNodo(ProApEsNodo);
			ExhibeFn(ProApEsNodo);
			cout<<endl;
                break;
 		case 2:
                	SuprimeNodoFinalFnApEsNodo(ProApEsNodo);
			ExhibeFn(ProApEsNodo);
			cout<<endl;
                break;
 		case 3:
                	ProApEsNodo=AgregaInicioFnApEsNodo(ProApEsNodo);
			ExhibeFn(ProApEsNodo);
			cout<<endl;
                break;
 		case 4:
                        AgregaFnApEsNodo(ProApEsNodo);
			ExhibeFn(ProApEsNodo);
			cout<<endl;
                break;
		case 5:
                        cout<<"Ingresa posicion a eliminar"<<endl;
	                cin>>posicion;
			ProApEsNodo=SuprimeNodoMedioFnApEsNodo(ProApEsNodo,posicion);
			ExhibeFn(ProApEsNodo);
			cout<<endl;
                break;
		case 6:
                        cout<<"Ingresa posicion a agregar"<<endl;
	                cin>>posicion;
			ProApEsNodo=AgregaNodoMedioFnApEsNodo(ProApEsNodo,posicion);    
			ExhibeFn(ProApEsNodo);
			cout<<endl;
                break;
            } 
	     cout<<"¿Regresar Al Menu [S/N]:?";
	      cin>>opcionCa;
	}
	while(opcionCa=='s');

}



*/

main(void){

char ExpAritmeticaCd[]={"*+abc"};
generaArbolFnApEsNodo(ExpAritmeticaCd);

}


#include<stdio.h>
#include<stdlib.h>
/*Obs1: Un FUNCION SE DEFINE COMO UNA DECLARACION EN CODIGO*/
//

void HolaFnVd(void);
int  BetaFnEn(int);
int *AlfaFnApEn(int);
int (*CreaFnApArEn(void))[2];
void ExhibeFn(int *);

//Comienza progrma
void HolaFnVd(void){
printf("Hola Mundo\n");
}

/*Regla 1: En nombre de una funcion es un apuntador constante a la funcion misma*/
int main(void){
//Prueba de Hola mundo
//HolaFnVd();
void(*pApFnVd)()=HolaFnVd; /*Regla 1*/
(*pApFnVd)();

//segunda parte

int *(*qApEnApEn)(int)=AlfaFnApEn;
printf("%d\n",*(*qApEnApEn));
int AEn=*AlfaFnApEn(1080);
printf("%d\n",AEn);

//tercera parte

//Prueba de impresion de arreglo de apuntadores antes de implementar ExhibeFnVd 
//int (*pApArEn)[2]=CreaFnApArEn();
//For que sirve como ExhibeFn
/*
for(int iEn=0;iEn<2;iEn++){
  printf("%d\n",(*pApArEn)[iEn]);
}
*/


//SI FUNCIONA SOLO MARCA WARNING, CON LOS WARNINGS SI PUEDE CONTINUAR, CON LOS ERRORES NO
ExhibeFnVd(*CreaFnApArEn());

  return 0;

}

int BetaFnEn(int AEn){
  return AEn;
}

int *AlfaFnApEn(int AEn){
  /*Nota: Una funcion que devuelve una estructura debe ser dinamica*/
/*porque?: porque una estructura dinamica no se destruye*/

   int *pApEn=new int;
  //"new" sirve como malloc pero en c++

//  int *pApEn=(int *)malloc(sizeof(int));

   *pApEn=AEn;
   return pApEn; 
}


int(*CreaFnApArEn(void))[2]{
 // int (*pApArEn)[2]=(int(*)[2])new int[2];
//valido en c++, en c:
    int (*pApArEn)[2]=(int(*)[2])malloc(sizeof(int)*2);  

 (*pApArEn)[0]=1;
   (*pApArEn)[1]=7;

   return  pApArEn;
}

void ExhibeFnVd(int (*pApArEn)[2]){
  for(int iEn=0;iEn<2;iEn++){
      printf("%d\n",(*pApArEn)[iEn]);
  }
}



//#####APUNTES#####
/*
REGLA 1
+---------+---------+----------+
|   C     |  malloc |   free   |
+---------+---------+----------+
|   C++   |  new    |  delete  |
+---------+---------+----------+

En "C" tenemos: malloc, free
En "C++" tenemos: malloc, free, new, delete
No se deben cruzar la funciones de alocacion y libreacion de memoria para c y c++
Nota: revisar si funcionan las 4 funciones de alocacion de memoria, y variables dinamicas

Apuntadores a puntadores a Enteros:

        +---+
pApEn-> | * |->5
        +---+
        | * |->3
        +---+
*/

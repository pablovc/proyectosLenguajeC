//Apuntadores en y a arreglos
//Notacion Mex**

#include<stdio.h>
//#include<stdlib.h>


int main(void){


//####PRIMERA PARTE DE LA CLASE####
/*
int AEn=8;
//A es un ENTERO A En es un identificador variable que se inicia en tiempo de compilacion. 
printf("Primer valor del entero %d\n",AEn);

int *pApEn=&AEn;
//p es un APUNTADOR a un ENTERO.
printf("apuntador a entero %d\n",pApEn);

*pApEn=10;
//el valor del entero AEn cambia a 10.
//El * desrreferencia al apuntador, es decir es la localidad de memoria misma.

printf("segundo valor del entero %d\n",AEn);

*/

//####SEGUNDA PARTE DE LA CALSE####

/*
//pApArApEn
//p -> calificador
//Ap -> tipo basico
//Ap,Ar,Ap -> tipos de apuntadores
// En -> tipo de la base

//int  *(*pApArApEn)[2];
//p es un Apuntador a un Arreglo de Apuntadores a Enteros
//p->|->En|->En|
//primer caracter calificador: p
//apuntador: tipo basico (Ap)
//tipos de apuntadores: a arreglo de apuntadores (ArAp)
//tipo de la base: Entero (En)

int xArEn[]={4,5,6};
int *pApEn=xArEn;
//El nombre de un arreglo es un apuntador CONSTANTE al primer elemento del mismo
//es decir, es un apuntador CONSTANTE a un entero
//pApEn->|->xArEn||
//xArEn=pApEn; esto no es valido porque intenta cambiar 
//la direccion base del arreglo que es constante

printf("Se muestra un arreglo de elementos enteros con apuntadores\n");
for(int i=0;i<3;i++){
    printf("%d",*(pApEn+i));
 
}
printf("\n");
for(int i=0;i<3;i++){
    printf("%d",pApEn[i]);
}
printf("\n");
*/
//####PRIMER EXPERIMENTO####

//XArApEn (X es un arreglo de apuntadores a ENTEROS)
//XArApAE: |*->En1|*->En2|

//Variables ENTEROS
int AEn=5;
int BEn=7;
//Apuntadores
int *pApEn=&AEn;
int *qApEn=&BEn;
//Arreglo de apuntadors con dos elementos
int *XArApEn[2]={pApEn,qApEn};

//Impresiones de pantalla de los 3 casos
//impresion directa de variables ENTEROS
printf("%d,%d\n",AEn,BEn);
//impresion de los valores de los apuntadores
printf("%d,%d\n",*pApEn,*qApEn);
//impresion de los elementos de los apuntadores
printf("%d,%d\n",*XArApEn[0],*XArApEn[1]);
*XArApEn[0]=10;
printf("%d\n",*XArApEn[0]);

//####SEGUNDO EXPERIMENTO####
//p es un apuntador a un arreglo de apuntadores
//p->|*->Ca1|*->Ca2|
char Ca1 = 'X';
char Ca2 = 'Y';

char *pApCa=&Ca1;
char *qApCa=&Ca2;

char *XArApCa[2]={pApCa,qApCa} ;


//para este apuntador se necesita hacer el tipo compatible de caracter y las llamadas correspondientes
char *(*pApArApCa)[2]=(char*(*)[2])XArApCa;
printf("%c,%c\n",*(*pApArApCa)[0],*(*pApArApCa)[1]);



return 0;
}



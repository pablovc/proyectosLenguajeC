
#include<iostream>
#include<stdio.h>

using namespace std;

struct EsNodo{
	//CaraCa es el campo del nodo selnialado por qApEsNodpo
	char CaraCa;
	EsNodo *SigApEsNodo;
};


EsNodo *IniciaFnApEsNodo(void){
	EsNodo *ProApEsNodo = new EsNodo;

	puts ("\nIngresa un caracter:");
	cin>>ProApEsNodo->CaraCa;
//Lista enlazada
	ProApEsNodo->SigApEsNodo=NULL;
//        ProApEsNodo->SigApEsNodo;

	return ProApEsNodo;
}


EsNodo *AgregaFnApEsNodo(EsNodo *ProApEsNodo){
	EsNodo *tApEsNodo=ProApEsNodo;
	EsNodo *pApEsNodo= new EsNodo;

	puts("\nIngresa un caracter:");
	cin>> pApEsNodo->CaraCa;
	while(tApEsNodo->SigApEsNodo!=NULL)
		tApEsNodo=tApEsNodo->SigApEsNodo;
	tApEsNodo->SigApEsNodo=pApEsNodo;
	pApEsNodo->SigApEsNodo=NULL;
}



EsNodo *AgregaInicioFnApEsNodo(EsNodo *ProApEsNodo){
	EsNodo *qApEsNodo= new EsNodo;
	EsNodo *pApEsNodo=ProApEsNodo;

	puts("\nIngresa un caracter:");
	cin>> qApEsNodo->CaraCa;

	qApEsNodo->SigApEsNodo=pApEsNodo;
	return qApEsNodo;
}



void ExhibeFn(EsNodo *ProApEsNodo){
	while(ProApEsNodo!=NULL){
		cout<< "  " << ProApEsNodo->CaraCa;
		ProApEsNodo=ProApEsNodo->SigApEsNodo;
	}
}


EsNodo *SuprimeNodoInicialFnApEsNodo(EsNodo *ProApEsNodo){

	EsNodo *pApEsNodo=ProApEsNodo;
	ProApEsNodo=ProApEsNodo->SigApEsNodo;
	pApEsNodo->SigApEsNodo=NULL;
	delete pApEsNodo;

	return ProApEsNodo;

}


EsNodo *SuprimeNodoFinalFnApEsNodo(EsNodo *ProApEsNodo){

	EsNodo *pApEsNodo=ProApEsNodo;
	EsNodo *qApEsNodo= new EsNodo;

	while(pApEsNodo->SigApEsNodo!=NULL){
		qApEsNodo=pApEsNodo;
		pApEsNodo=pApEsNodo->SigApEsNodo;

	}

	qApEsNodo->SigApEsNodo=NULL;
	delete pApEsNodo;
}


EsNodo *SuprimeNodoMedioFnApEsNodo(EsNodo *ProApEsNodo, int pos){

	EsNodo *pApEsNodo=ProApEsNodo;
	EsNodo *qApEsNodo= new EsNodo;
	int ContaNodos=0;

	if(pos==0){
		return SuprimeNodoInicialFnApEsNodo(pApEsNodo);
	}

	if(pos<0){
		cout<<"Posicion no vAlida"<<endl;
		return pApEsNodo;
	}

	while(pApEsNodo->SigApEsNodo!=NULL&&ContaNodos!=pos){
		qApEsNodo=pApEsNodo;
		pApEsNodo=pApEsNodo->SigApEsNodo;
		ContaNodos++;
	}

	qApEsNodo->SigApEsNodo=pApEsNodo->SigApEsNodo;
	delete pApEsNodo;

	return ProApEsNodo; 
}


EsNodo *AgregaNodoMedioFnApEsNodo(EsNodo *ProApEsNodo, int pos){

	EsNodo *pApEsNodo=ProApEsNodo;
	EsNodo *qApEsNodo= new EsNodo;

	int ContaNodos=0;

	puts("\nIngresa un caracter:");
	cin>> qApEsNodo->CaraCa;

	while(pApEsNodo->SigApEsNodo!=NULL&&ContaNodos!=pos){
		pApEsNodo=pApEsNodo->SigApEsNodo;
		ContaNodos++;
	}

	qApEsNodo->SigApEsNodo=pApEsNodo->SigApEsNodo;
	pApEsNodo->SigApEsNodo=qApEsNodo;

	return ProApEsNodo; 
}




main(void){
	int tamEn;	// Referencia temporal para crear la lista

       cout<<"Programa Listas"<<endl;
	
	cout<<"Ingresa tamanio de lista"<<endl;
	cin>>tamEn;

        cout<<"Ingresa caracter por caracter los elementos de la lista"<<endl;

	EsNodo *ProApEsNodo=IniciaFnApEsNodo();

	for(int iEn=0; iEn<tamEn; iEn++){
		AgregaFnApEsNodo(ProApEsNodo);
	}

        cout<<"La nueva lista es:"<<endl;
	ExhibeFn(ProApEsNodo);
	cout<<endl;
       
          

	char opcionCa;
        int menuOpcion;
        int posicion;

	do{
        //borrar pantalla
        cout << "\033[2J\033[1;1H";
        ExhibeFn(ProApEsNodo);
	cout<<endl;
	cout<<"Menu de opciones"<<endl;
	cout<<"1 -> suprimir nodo al inicio"<<endl;
	cout<<"2 -> suprimir nodo al final"<<endl;
	cout<<"3 -> agregar nodo al inicio"<<endl;
	cout<<"4 -> agregar nodo al final"<<endl;
	cout<<"5 -> suprimir nodo en posicion N"<<endl;
	cout<<"6 -> agregar nodo en posicion N"<<endl;
	cout<<endl; 
        cout<<"Ingresa entero referido al menu de opciones"<<endl;
	cin>>menuOpcion;
       //system(clear);
	    switch(menuOpcion){
                case 1:
                        ProApEsNodo=SuprimeNodoInicialFnApEsNodo(ProApEsNodo);
			ExhibeFn(ProApEsNodo);
			cout<<endl;
                break;
 		case 2:
                	SuprimeNodoFinalFnApEsNodo(ProApEsNodo);
			ExhibeFn(ProApEsNodo);
			cout<<endl;
                break;
 		case 3:
                	ProApEsNodo=AgregaInicioFnApEsNodo(ProApEsNodo);
			ExhibeFn(ProApEsNodo);
			cout<<endl;
                break;
 		case 4:
                        AgregaFnApEsNodo(ProApEsNodo);
			ExhibeFn(ProApEsNodo);
			cout<<endl;
                break;
		case 5:
                        cout<<"Ingresa posicion a eliminar"<<endl;
	                cin>>posicion;
			ProApEsNodo=SuprimeNodoMedioFnApEsNodo(ProApEsNodo,posicion);
			ExhibeFn(ProApEsNodo);
			cout<<endl;
                break;
		case 6:
                        cout<<"Ingresa posicion a agregar"<<endl;
	                cin>>posicion;
			ProApEsNodo=AgregaNodoMedioFnApEsNodo(ProApEsNodo,posicion);    
			ExhibeFn(ProApEsNodo);
			cout<<endl;
                break;
            } 
	     cout<<"¿Regresar Al Menu [S/N]:?";
	      cin>>opcionCa;
	}
	while(opcionCa=='s');


/*
	ProApEsNodo=SuprimeNodoInicialFnApEsNodo(ProApEsNodo);
	ExhibeFn(ProApEsNodo);
	cout<<endl;

	SuprimeNodoFinalFnApEsNodo(ProApEsNodo);
	ExhibeFn(ProApEsNodo);
	cout<<endl;

	ProApEsNodo=AgregaInicioFnApEsNodo(ProApEsNodo);
	ExhibeFn(ProApEsNodo);
	cout<<endl;


	ProApEsNodo=SuprimeNodoMedioFnApEsNodo(ProApEsNodo,2);
	ExhibeFn(ProApEsNodo);
	cout<<endl;

	ProApEsNodo=AgregaNodoMedioFnApEsNodo(ProApEsNodo,1);    
	ExhibeFn(ProApEsNodo);
	cout<<endl;
*/

}


//#### PREAMBULO ####
#include<stdio.h>
#include<iostream>
#include<stdlib.h>

using namespace std;

/*
Pilas
Una pila es una estructura de datos lineal con dos operaciones por un mismo extremo: agregar y remover

PilaArEn

int PilaArEn[6];

    +---+
n-1 |   | <-TopEn=n-1
    +---+
    |...|
    +---+
    |101|<-TopeEn
    +---+
    |...|
    +---+
    |101|<-TopeEn=-1
    +---+
 
*/

//Variables globales
/*
int TopeEn=-1;
int PilaArEn[12];
int NoMaxItemsEn=12;
*/

class CsPilas{

public:
    void AgregaFaFn(int);
    int RemueveEnFaFn(void);
   void ExhibePilaFnFa(void);
    CsPilas(void);
private: 
   int TopeEn;
   int PilaArEn[12];
   int NoMaxItemsEn;
//FUNCIONES DE EXAMEN
 //int *CreaFnApEn(void);
  //bool EsVaciaFaBo(void);
  //bool EsLlenaBo(void);
};
//Fin de la clase

//Constructor agrega valores
CsPilas::CsPilas(void){
   TopeEn=-1;
   NoMaxItemsEn=12;
}

//int *CreaFnApEn(void);


//EXAMEN PARCIAL MARTES
//REALIZAR EL TOPE ENTERO CON APUNTADORES
/*

        +---+
TopeEn->| * |->1  CraFnApEn(void){Ingresar entero}
        +---+
        | * |->7
        +---+
        |...|
        +---+
*/


//

//Funcion Asociada de agregar elementos
void CsPilas::AgregaFaFn(int AlfaEn){
  if(TopeEn!=NoMaxItemsEn-1){
    PilaArEn[++TopeEn]=AlfaEn;
    }
   else{
        cout<<"Pila desbordada"<<endl;
        exit(1);
   }

}

//Funcion Asociada de destruccion de elementos
int CsPilas::RemueveEnFaFn(void){
    //if(EsVaciaFnBo());
    if(TopeEn!=-1){
      return PilaArEn[TopeEn--];
    }
    else{
      cout<<endl;
      cout<<"Pila vacia"<<endl;
      exit(1);
    }
}   

void CsPilas::ExhibePilaFnFa(void){
    //se copia valor de TopeEn original para no moverlo
     int Tope0En=TopeEn;
     cout<<"+---+"<<endl;
     cout<<"| "<<PilaArEn[Tope0En--]<<" |"<<"<-TopeEn"<<endl;
     cout<<"+---+"<<endl;
     for(int iEn=Tope0En;iEn>-1;iEn--){
       cout<<"| "<<PilaArEn[Tope0En--]<<" |"<<endl;
       cout<<"+---+"<<endl; 
     }
}


int main (void){

//Creacion de objeto
CsPilas LaPilaEn;


//CASO PILA Ejemplo
 int NumeroElementos;
 int RemueveElementos; 
char opcionCa;

//CASO PILA VACIA
//int NumeroElementos=4;
//int RemueveElementos=5;


do{

//Borra pantalla
system("clear");

cout<<"Numero de Elementos:";
cin>>NumeroElementos;
cout<<endl;

cout<<"Elementos a remover:";
cin>>RemueveElementos;
cout<<endl;

//Borra pantalla
system("clear");

//Se agregan elementos
for(int iEn=0;iEn<NumeroElementos;iEn++)
   LaPilaEn.AgregaFaFn(iEn);


//Se exhibe la pila con todos los elementos
LaPilaEn.ExhibePilaFnFa();

//salto de linea para visibilidad
cout<<endl;

//Se borran elementos
for(int iEn=0;iEn<RemueveElementos;iEn++)
//LaPilaEn.RemueveEnFaFn();
 cout<<LaPilaEn.RemueveEnFaFn()<<"";


//salto de linea para visibilidad
cout<<endl;

//Se exhibe la pila con elementos elmiminados
LaPilaEn.ExhibePilaFnFa();

//Aqui esta la impresion provisional de la pila removida
  // cout<<LaPilaEn.RemueveEnFaFn()<<"";

//Se borra pantalla
//system("clear");
cout<<"otro ejemplo:";
cin>>opcionCa;
}while(opcionCa=='s');



//salto de linea para visibilidad
cout<<endl;
return 0;
}

#include<iostream>
#include<stdlib.h>
#include<stdio.h>
using namespace std;

void LeeOpdoFn(int OpdoAAr[7]){
	cout<<"Ingresa 6 numeros binarios:"<<endl;
	OpdoAAr[0]=0;
	cin>>OpdoAAr[1]>>OpdoAAr[2]>>OpdoAAr[3]>>OpdoAAr[4]>>OpdoAAr[5]>>OpdoAAr[6];
}

void SumaBinariaFn(int OpdoAAr[7], int OpdoBAr[7],int OpdoRAr[7]){
	int DigitoAEn, DigitoBEn;
	int AcarreoEn=0;
	for(int iEn=6;iEn>=0;iEn--){
		DigitoAEn=OpdoAAr[iEn];
		DigitoBEn=OpdoBAr[iEn];
		if(DigitoAEn==1){
			if(DigitoBEn==1){
				if(AcarreoEn==1){
					OpdoRAr[iEn]=1;
					AcarreoEn=1;
				}
				else{
					OpdoRAr[iEn]=0;
					AcarreoEn=1;
				}
			}
			else{
				if(AcarreoEn==1){
					OpdoRAr[iEn]=0;
					AcarreoEn=1;
				}
				else{
					OpdoRAr[iEn]=1;
					AcarreoEn=0;			
			}
			
		}
	}
	else{
		if(DigitoBEn==1){
				if(AcarreoEn==1){
					OpdoRAr[iEn]=0;
					AcarreoEn=1;
				}
				else{
					OpdoRAr[iEn]=1;
					AcarreoEn=0;
				}
			}
			else{
				if(AcarreoEn==1){
					OpdoRAr[iEn]=1;
					AcarreoEn=0;
				}
				else{
					OpdoRAr[iEn]=0;
					AcarreoEn=0;			
			}
			
		}
	}
}
}

void ExhibeFn(int OpdoRAr[7]){
	for(int iEn=0;iEn<7;iEn++){
		cout<<OpdoRAr[iEn]<<" ";}
	cout<<endl;
}

main(void){
	int OpdeAEn[7]={0,1,1,1,0,1,1};
	int OpdeBEn[7]={0,0,0,1,1,1,0};
	int OpdeREn[7];
	char OpcionCa;
	
	do{
		LeeOpdoFn(OpdeAEn);
		LeeOpdoFn(OpdeBEn);
		cout<<endl;
		SumaBinariaFn(OpdeAEn,OpdeBEn,OpdeREn);
		ExhibeFn(OpdeAEn);
		ExhibeFn(OpdeBEn);
		cout<<endl;
		ExhibeFn(OpdeREn);
		cout<<"\nQuieres intentarlo de nuevo? s/n: ";
		cin>>OpcionCa;
		system("cls");
	}while(OpcionCa == 's');
}
